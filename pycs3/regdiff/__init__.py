"""
Regression difference curve shifting technique, using gaussian process regression.
"""

__all__ = ["rslc", "pymcgp", "multiopt"]
